package com.capgemini.TestLinkedInWithMaven;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestLinked {

	//do testowania zostanie użyta biblioteka selenium
	//w celu sterowania przeglądarka nalezy pobrać odpowiedni sterownik na dysk
	
	//do sterowania przeglądarka pomocny jest interface WebDriver
	//stwórzmy instancję klasy WebDriver
	//jest podkreślony na czerwony gdyż nie ma w projekcie biblioteki w której znajduje się wskazany interface
	private WebDriver webDriver;
	
	//przed wywołaniem każdej metody jest potrzebne dodanie sterownika do właściwości systemu oraz 
	//inicjalizacji odpowiedniego rodzaju przeglądarki
	//na potrzeby tego testu została wybrana przeglądarka chrome
	@Before
	public void init() {
		//w celu uzyskania nazwy zmiennej środowiskowej możemy wywołać błąd podczas testu
		String workingDirectory = System.getProperty("user.dir");
		String separator = System.getProperty("file.separator");
		String nameOfDriver = "";
		if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
			nameOfDriver ="chromedriver";
		}
		else {
			nameOfDriver = "chromedriver.exe";
		}
//		System.setProperty("webdriver.chrome.driver", workingDirectory+separator+"resources"+separator+nameOfDriver);
		webDriver = new ChromeDriver();
		//na potrzeby tych testów zakładam że wszystkie testy rozpoczynam od strony startowej linkedin
		webDriver.get("https://www.linkedin.com/");
	}
	
	@Test
	public void sprawdzenie_czy_dziala_poprawnie_przekierowanie_po_wyslaniu_maila_z_przypomnieniem() {
		WebElement przypomnijHaslo = webDriver.findElement(By.xpath("//*[@id=\"layout-main\"]/div/div[1]/div/form/a"));
		przypomnijHaslo.click();
		
		WebElement uzupelnijMaila = webDriver.findElement(By.id("userName-requestPasswordReset"));
		uzupelnijMaila.sendKeys("b2bcapgemini@gmail.com");
		
		WebElement wyslijMaila = webDriver.findElement(By.id("btnSubmitResetRequest"));
		wyslijMaila.click();
		
		assertEquals("https://www.linkedin.com/uas/request-password-reset-submit", webDriver.getCurrentUrl());
	}
	@After
	public void close() {
		webDriver.quit();
	}

}
